<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

// Routes for Products
Route::get('/', 'ProductController@index');
Route::resource('products', 'ProductController');

//Routes for Users
Route::get('/home', 'HomeController@index');
Route::get('account', 'AccountSettingsController@index');
Route::post('account', 'AccountSettingsController@updateAccount');

//Routes for Wishlist
Route::post('wishlist/{product}',['as' => 'wishlist.update', 'uses' => 'WishlistController@update']);
Route::get('wishlist', 'WishlistController@index');
Route::post('wishlist/delete/{id}', ['as' => 'wishlist.destroy', 'uses' => 'WishlistController@destroy']);