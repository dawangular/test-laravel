<?php
$date = new \DateTime('now');
$date = date_format($date,'Y-m-d H:i:s');


return array (
    0 =>
        array (
            'id' => 1,
            'name' => 'Amica 7kg White Freestanding Vented Tumble Dryer ADV7CLCW',
            'price' => '199.95',
            'description' => 'Amica 7kg Tumble Dryer ADV7CLCW 
                            Features include delay start, child safety lock, and a choice of 12 programmes. 
                            Vented dryers are the traditional type of dryer and are generally the cheapest to run as they use less energy than condenser dryers.
                            
                            Dimensions (W x H x D): 595 mm x 840 mm x 550 mm
                            
                            Warranty: 2 Years Parts and Labour',
            'urlImage' => '1.png',
            'category' => 'Laundry Appliances',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    1 =>
        array (
            'id' => 2,
            'name' => 'White Knight 3.5kg White Freestanding Vented Tumble Dryer WK39AW',
            'price' => '189.95',
            'description' => 'The WK39AW compact dryer has the added benefit of reverse tumble action, which can reduce tangling and ironing time. A perfect choice for the smaller house or apartment where space is at a premium.
                                Dimensions ( w x h x d): 670mm x 500mm x 470mm**',
            'urlImage' => '2.png',
            'category' => 'Laundry Appliances',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    2 =>
        array (
            'id' => 3,
            'name' => 'Montpellier 5kg 1000rpm White Freestanding Washing Machine MW5100P',
            'price' => '169.95',
            'description' => 'With its neat and compact design, the MW5100P is ideal for your home. Best suited to a smaller family, this model will have your clothes looking and smelling their best.
                              Family Friendly
                              The MW5100P was designed with you in mind, that’s why the 15 programmes are all tailored to meet your family\'s needs.
                              If you\'ve had a busy day then the Quick Wash Option is for you. Capable of washing up to 2.5kg in just 30 minutes, it’s perfect for those on the go.
                              The MW5100P also boasts an impressive Sportswear Option. Ideal for those looking to get their gym clothes smelling brand new once again!
                              Whatever your needs, Montpellier has you covered. There are a range of other programmes including Cotton, Pre-wash, Delicate and Blouses/Shirt.
                              The handy Delay Timer and LED Progress Bar give you complete control over your washes – leaving you time for the more important things.
                              Rapid Delivery
                              We know you\'re busy, that’s why we offer a rapid delivery and installation service. And with our Price Match Promise you can rest easy knowing you got the best deal out there. So don’t delay, order online today!',
            'urlImage' => '3.png',
            'category' => 'Laundry Appliances',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    3 =>
        array (
            'id' => 4,
            'name' => 'Iceking 42L White Freestanding Tabletop Fridge TT46AP2',
            'price' => '109.95',
            'description' => 'The TT46AP2 is a plain white unbranded mini fridge and is an ideal alternative to the popular beer and drinks branding. With its solid door design it\'s ideal for food and dairy. With an internal shelf as standard you can easily store drinks, food or chocolate. The temperature can be easily adjusted via the thermostat located at the back of the chiller. With its compressor based technology it also has an A+ rating for energy efficiency. Not only this but you get maximum flexibility as this item also includes an Ice Box!
                              2 Years Parts and Labour',
            'urlImage' => '4.png',
            'category' => 'Fridges & Freezers',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    4 =>
        array (
            'id' => 5,
            'name' => 'Bosch 373/200L American Fridge Freezer KAN90VI20G',
            'price' => '1589.95',
            'description' => 'The Bosch KAN90VI20G is an stainless steel American non plumbed Fridge Freezer which has an energy rating of A+. It is frost free which means there is a constant flow of cold air throughout the appliance preventing it from a build up of ice. It has super fast freezing and cooling and has magnetic doors.
                              The open door alarm lets you know when the door has been left open and it has an LED light.
                              This product comes with a 2 year warranty.',
            'urlImage' => '5.png',
            'category' => 'Fridges & Freezers',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    5 =>
        array (
            'id' => 6,
            'name' => 'Maytag 70/30 Stainless Steel Freestanding American Fridge Freezer 5MFI267AA',
            'price' => '2299.95',
            'description' => 'The Maytag 5MFI267AA French style fridge freezer offers a huge 449 litres of usable space in the fridge and a further 122 litres in the freezer. This model features a plumbed in ice & water dispenser in the fridge door and boasts an A+ energy rating. Please be aware that the depth of this model is 89cm which is substantially deeper than most other models.
                              Warranty: 2 Years Labour 10 Year Parts
                              Please note: For items marked PreOrder, we will endeavour to have the products delivered to you in 5 – 7 working days. ',
            'urlImage' => '6.png',
            'category' => 'Fridges & Freezers',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    6 =>
        array (
            'id' => 7,
            'name' => 'Beko 12 Place Setting Silver Freestanding 14L Dishwasher DFC04210S',
            'price' => '269.95',
            'description' => 'Full Size Dishwasher with A energy rating and 4 Programmes.
                              2 Years Parts and Labour - Warranty Subject to Online Registration. See T&Cs on www.beko.ie/register.',
            'urlImage' => '7.png',
            'category' => 'Dishwashers',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    7 =>
        array (
            'id' => 8,
            'name' => 'Montpellier 10 Place Setting White Freestanding Slimline 10.5L Dishwasher DW1064P',
            'price' => '239.95',
            'description' => 'The ten place setting DW1064P, 45cm Slimline dishwasher is simple and straightforward. No complicated settings or gadgets; simply load the machine, select the required program and away you go. Simple in form and function.',
            'urlImage' => '8.png',
            'category' => 'Dishwashers',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    8 =>
        array (
            'id' => 9,
            'name' => 'Montpellier 12 Place Settings Freestanding 12L Dishwasher DW1254P',
            'price' => '209.95',
            'description' => 'Efficient and modern, the Montpellier DW125P is the perfect dishwasher for your busy family.
                              Easy Clean
                              Simple and easy to use, this no frills dishwasher will make sure you never have to hand wash dishes again. The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience.
                              If time is against you, simply select the Quick Wash Option and your dishes will be sparkling clean in just 30 minutes.
                              If your dishes need a more thorough wash, then no look no further than the Intensive Option. This programme is ideal for cleaning up after family meals or dinner parties.
                              Buy in Confidence
                              Whichever programme you select, you can rest easy knowing that the DW1254P is operating at the highest level of efficiency. Rated A+ for energy, this model is kinder to both your bills and the environment!
                              So order online today. With our Price Match Promise and rapid delivery service, this stylish Montpellier dishwasher could be yours in no time.',
            'urlImage' => '9.png',
            'category' => 'Dishwashers',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    9 =>
        array (
            'id' => 10,
            'name' => 'Tower 800W 20L Microwave Black T24009',
            'price' => '69.95',
            'description' => 'Tower microwave oven
                            800w
                            20 litre
                            Manual control
                            Black & chrome
                            6 power levels ',
            'urlImage' => '10.jpg',
            'category' => 'Microwaves',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    10 =>
        array (
            'id' => 11,
            'name' => 'Bosch 800W Freestanding Microwave HMT75M461B',
            'price' => '89.95',
            'description' => 'Serie | 4 Compact microwave oven
                            Freestanding microwave that defrosts, heats and cooks helping you prepare food in the kitchen.
                            AutoPilot 7: every dish is a perfect success thanks to 7 pre-set automatic programmes.
                            Automatic weight option: choose the programme and enter the weight and the microwave oven will do the rest.
                            2 Years Parts and Labour',
            'urlImage' => '11.jpg',
            'category' => 'Microwaves',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    11 =>
        array(
            'id' => 12,
            'name' => 'Bosch 900W 25L Built-in Microwave HMT84M654B',
            'price' => '399.95',
            'description' => 'Serie | 6 Compact microwave oven
                              AutoPilot 7: every dish is a perfect success thanks to 7 pre-set automatic programmes.
                              Frameless design: easy installation, harmonious integration into the kitchen furniture and easy to clean.
                              Side-opening door: convenient opening thanks to left-hinged door.
                              Create a contemporary finish to your kitchen with a black glass front.
                              2 Years Parts and Labour',
            'urlImage' => '12.jpg',
            'category' => 'Microwaves',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    12 =>
        array(
            'id' => 13,
            'name' => 'Montpellier 900mm Freestanding Gas Range Cooker MR90GOX',
            'price' => '799.95',
            'description' => 'Montpellier Freestanding Gas Range Cooker MR90GOX.
                                Avoid the hassle of buying multiple appliances by splashing out on a stylish range!
                                Equipped with 5 gas burners including wok style centre burner and cast-iron supports.
                                Large single gas oven with LED display.
                                Perfect for those who cater for large groups regularly.

                                This machine comes set-up to run on natural gas with the bonus of being fully convertible to run off LPG.
                                (Please ensure you let our customer support team know if you require LPG jets)

                                Comes with a 2-year warranty, parts, and labour.

                                Please note: For items marked PreOrder, we will endeavour to have the products delivered to you in 5 – 7 working days. ',
            'urlImage' => '13.png',
            'category' => 'Cooking',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    13 =>
        array(
            'id' => 14,
            'name' => 'Beko 600mm Electric Hob HIZE64101W',
            'price' => '99.95',
            'description' => 'The Beko 600mm integrated sealed plate hob with four heating zones and 1 to 6 cooking levels - cooking, boiling, and frying a range of food at once is easy!',
            'urlImage' => '14.jpg',
            'category' => 'Cooking',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    14 =>
        array(
            'id' => 15,
            'name' => 'Amica 500mm Conventional Hood Stainless Steel OSC5468W',
            'price' => '69.95',
            'description' => 'Amica 500mm Conventional Hood OSC5458I with white finish.
                            Featuring sleek, white exterior and soft halogen lighting.
                            Comes with an acrylic grease filter.
                            3-speed settings to ensure you always have the right amount of suction.

                            Dimensions (W x H x D): 500mm x 130mm x 505mm.
                            Warranty: 2 years. ',
            'urlImage' => '15.jpg',
            'category' => 'Cooking',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    15 =>
        array(
            'id' => 16,
            'name' => 'Sonoro Stereo Red Stereo CD FM DAB+ Radio with Bluetooth SO3100100RD',
            'price' => '699.95',
            'description' => 'CD player with repeat, shuffle and Resume Function
                                FM and DAB + tuner with 6 station presets for easy radio reception
                                Brightness sensor for automatic adjustment of the brightness of the display to the ambient conditions and extremely sensitively adjustable display brightness for trouble-free night-time operation
                                Modern, audio electronics and speaker technology
                                adjustable subwoofer
                                Stable housing made of MDF and solid front panel and Speaker bezel made of real aluminum
                                Preloaded Wecksounds (bird calls, etc.), the waking up gently with slowly increasing volume
                                USB port for charging mobile devices such as smartphone or tablet
                                Headphone jack, remote**',
            'urlImage' => '16.png',
            'category' => 'Small Appliances',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    16 =>
        array(
            'id' => 17,
            'name' => 'Dyson Animal Cinetic Big Ball Vacuum Cleaner DC75',
            'price' => '419.95',
            'description' => 'All others lose suction. Dyson Cinetic science doesnt. For homes with pets.
                                - Dyson Cinetic science - captures the dust that clogs all other vacuums. No dirty filters to wash or replace.
                                - Self-adjusting cleaner head seals in suction across carpets and hard floors. With suction control for difficult tasks. Our machines adjust automatically to every floor type picking up dust others leave behind.
                                - 20 more brush bar power for strong suction on all floors.2
                                - Increased power drives bristles deeper, to release more dirt.
                                - Ball technology - streamlined profile for easy steering around the home. Our vacuums can be steered around furniture and into difficult places with a simple turn of the wrist.
                                - Expels cleaner air than any other cyclonic vacuum.
                                - Instant high reach wand. Stretches to the top of the stairs - no cumbersome parts to lift-away and carry.
                                - Our hose and wand release in one smooth action, making it easy to clean up high.
                                - Includes hygienic bin emptying, crevicebrush and stair tools.
                                - Tangle-free Turbine tool. The only turbine tool that removes ground-in dirt and hair without the tangles.
                                - 5 year guarantee.
                                ',
            'urlImage' => '17.png',
            'category' => 'Small Appliances',
            'created_at' => $date,
            'updated_at' => $date,
        ),
    17 =>
        array(
            'id' => 18,
            'name' => 'Dyson Animal Vacuum Cleaner DC54 ',
            'price' => '369.95',
            'description' => 'Dyson Cinetic Animal 2015 is the first vacuum cleaner with Dyson Cinetic cyclone technology 54 ultra-efficient cyclones extract more microscopic particles than any other cyclone. Theres no filter that needs washing or replacing and with no bag, there are no extra costs. Dyson Cinetic also has Ball technology and a central steering mechanism, so it turns on the spot and follows with greater control. Its reconfigured carbon fibre turbine head combines carbon fibre brushes to clean hard floors and nylon bristles to remove ground-in dirt from carpets. Engineered for tough tasks, Dyson Cinetic also comes with extra tools to remove hair and stubborn dirt. And Dyson Cinetic has a 5 year guarantee covering parts and labour.',
            'urlImage' => '18.png',
            'category' => 'Small Appliances',
            'created_at' => $date,
            'updated_at' => $date,
        ),
);