<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'description',
        'urlImage',
        'category'
    ];

    public function getPriceOutputAttribute()
    {
        return '€'. number_format($this->price, 2);
    }

    public function scopeSearch($query, $name)
    {
        return $query->where('name', 'LIKE', "%$name%");
    }

    public function wishlists()
    {
        return $this->belongsToMany(Wishlist::class);
    }
}
