<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\User;
use Auth;


class AccountSettingsController extends Controller
{
    /**
     * @return $this
     */
    public function index()
    {
        $user = Auth::user();
        return view('AccountSettings/index')->with('user', $user);
    }

    /**
     * @return $this
     */
    public function updateAccount()
    {
        $user = Auth::user();
        $user->name = Request::input('name');
        $user->email = Request::input('email');
        $user->address = Request::input('address');
        $user->country = Request::input('country');
        $user->town = Request::input('town');
        $user->postcode = Request::input('postcode');
        $user->phone = Request::input('phone');
        if ( ! Request::input('password') == '')
        {
            $user->password = bcrypt(Request::input('password'));
        }
        $user->save();
        return view('/home')->with('message', 'Your account has been updated!');
    }

}
