<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('filter') == 'low') {
            $products = Product::Search($request->name)->orderBy('price', 'asc')->get();
        }
        else{
            $products = Product::Search($request->name)->orderBy('price', 'desc')->get();
        }
        $products->each(function ($products) {
            $products->name;
            $products->description;
            $products->price;
            $products->urlImage;
            $products->category;
        });

        $user = Auth::id();
        $user_id = User::find($user);
        if(isset($user)){
            $wishes = $user_id->wishlist->products()->get();
            if(count($products) == 0){
                return view('products.index')->with(['products' => $products, 'selected' => $request->get('filter'), 'message' => 'Sorry, no data found', 'wishes' => $wishes ]);
            }
            else {
                return view('products.index')->with(['products' => $products, 'selected' => $request->get('filter'), 'wishes' => $wishes]);
            }
        }
        else{
            if(count($products) == 0){
                return view('products.index')->with(['products' => $products, 'selected' => $request->get('filter'), 'message' => 'Sorry, no data found' ]);
            }
            else {
                return view('products.index')->with(['products' => $products, 'selected' => $request->get('filter')]);
            }
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'urlImage' => 'nullable'
        ]);

        $product = new Product($request->all());

        //  To put in production I was use this name to save photos and no repeat names (For example)
        //  $imageName = time().'-'.$image->getClientOriginalName();
        if ($request->hasFile('urlImage')) {
            $image = $request->file('urlImage');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('img'), $imageName);
            $product->urlImage = $imageName;
        }
        $product->save();

        return view('products.index')->with('products', Product::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function redirectToProduct($product)
    {
        return redirect(route('product.show', $product->id));
    }
}
