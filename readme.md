# Laravel Test - Ignacio Pérez Molina

This is a test exercise using laravel

## Installation

Clone the proyect with:

    git clone git@bitbucket.org:dawangular/test-laravel.git

Open folder and install:

    composer install

Install dependencies with

    npm install

Create a new database in your environment 

Copy file ".env.example" and paste it in the same route with the name ".env" and configure to your own data 

Example:

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=database
    DB_USERNAME=admin
    DB_PASSWORD=1234
    
To change the Application key enter in your console:

    php artisan key:generate

Create tables and columns with the command:

    php artisan migrate

Finally to create a products list enter in the console:

    phpunit

## Video Installation

You can see install example and a little briefing in the next link

[Video example.](https://youtu.be/CzVJpG7goT0)