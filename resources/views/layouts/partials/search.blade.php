{!! Form::open(['route' => 'products.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
<div class="input-group">
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Search product...', 'aria-describedby' => 'search']) !!}
    <span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
</div>
{!! Form::close() !!}