<strong>Order</strong>
<div class="btn-group">
    {!! Form::open(['route' => 'products.index', 'method' => 'GET', 'id' => 'filter', 'class' => 'navbar-form pull-left'])!!}
    {{ Form::select('filter', [
        'high' => 'Price High to Low',
        'low' => 'Price Low to High']
    ,$selected, ['class' => 'form-control', 'onchange'=> 'this.form.submit()', 'id' => 'filter'])}}
    {!! Form::close() !!}
</div>

