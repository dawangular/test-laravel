@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="product-detail">
            <div class="container-fluid">
                <div class="wrapper row">
                    <div class="preview col-md-6">
                        <div class="preview-pic tab-content">
                            <div class="tab-pane active" id="pic-1"><img src="../img/{{$product->urlImage}}" /></div>
                        </div>
                    </div>
                    <div class="details col-md-6">
                        <h3 class="product-title">{{$product->name}}</h3>
                        <p class="product-description">{{$product->description}}</p>
                        <h4 class="price text-center">{{$product->priceOutput}}</h4>
                        <div class="action">
                            <a class="btn btn-default" href="#">Add wish list</a>
                            <a class="btn btn-success" href="#">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection