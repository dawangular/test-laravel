@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Add a Product</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
                              action="{{ route('products.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" placeholder="Name">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>
                                <div class="col-md-6">
                                    <textarea id="description" type="text" class="form-control" name="description"
                                              placeholder="Description"></textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Price</label>
                                <div class="col-md-6">
                                    <input id="price" type="text" class="form-control" name="price" placeholder="Price">
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="category" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6">
                                    {{ Form::select('Category', [
                                       'Laundry Appliances' => 'Laundry Appliances',
                                       'Dishwashers' => 'Dishwashers',
                                       'Fridges & Freezers' => 'Fridges & Freezers',
                                       'Microwaves' => 'Microwaves',
                                       'Cooking' => 'Cooking',
                                       'Small Appliances' => 'Small Appliances']
                                    ,null, ['class' => 'form-control'])}}
                                    @if ($errors->has('category'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('urlImage') ? ' has-error' : '' }}">
                                <label for="urlImage" class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    <input id="urlImage" type="file" class="form-control" name="urlImage">
                                    @if ($errors->has('urlImage'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('urlImage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection