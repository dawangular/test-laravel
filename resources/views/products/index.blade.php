@extends('layouts.app')

@section('content')
    <section class="jumbotron text-xs-center">
        <div class="container">
            <h1 class="jumbotron-heading">Welcome to Laravel</h1>
            <p class="lead text-muted">Example activity to work</p>
        </div>
    </section>
    <div class="container">
        <div class="well well-sm">
            @include('layouts.partials.order')
            @include('layouts.partials.search')
        </div>
        @if(isset($message))
            <div class="alert alert-warning">
                {{$message}}
            </div>
        @else
            <div id="products" class="row list-group">
                <div class="clearfix visible-xs"></div>
                @foreach($products as $product)
                    <div class="item  col-lg-4 col-sm-6 col-md-4 col-xs-12 vcenter">
                        <div class="thumbnail">
                            <a href="{{url('products/'.$product->id)}}">
                                <img class="group list-group-image" src="img/{{$product->urlImage}}" alt=""/>
                            </a>
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading"><a
                                            href="{{url('products/'.$product->id)}}">{{$product->name}}</a></h4>
                                <p class="group inner list-group-item-text">{{str_limit($product->description, 120)}}</p>
                                <div class="row">
                                    <p class="price-list">{{$product->priceOutput}}</p>
                                </div>
                                <div class="row">
                                    <div class="text-center col-xs-12 col-md-6">
                                        @if(isset($wishes))
                                            <?php $i = 0; ?>
                                            @foreach($wishes as $wish)
                                                @if($wish->id == $product->id)
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <button class="btn btn-warning disabled">
                                                                Added!
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php $i++; ?>
                                                @endif
                                            @endforeach
                                            @if($i == 0)
                                                {!! Form::open(['route' => ['wishlist.update', $product->id],'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            Add wish list
                                                        </button>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            @endif
                                        @else
                                            {!! Form::open(['route' => ['wishlist.update', $product->id],'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Add wish list
                                                    </button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        @endif

                                    </div>
                                    <div class="text-center col-xs-12 col-md-6">
                                        <a class="btn btn-success" href="#">Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection