@extends('layouts.app')

@section('content')
    <section class="jumbotron text-xs-center">
        <div class="container">
            <h1 class="jumbotron-heading">Welcome to Laravel</h1>
            <p class="lead text-muted">Example activity to work</p>
        </div>
    </section>
    <div class="container">
        <div class="well well-sm">
            <strong>Order</strong>
            <div class="btn-group">
                {!! Form::open(['route' => 'products.index', 'method' => 'GET'])!!}
                {{ Form::select('Category', [
                                       'low' => 'Price Low to High',
                                       'high' => 'Price High to Low']
                                    ,null, ['class' => 'form-control'])}}


                {{--{!! Form::open(['route' => 'products.index', 'method' => 'GET'])!!}--}}
                {{--<select class="form-control" id="order">--}}
                    {{--<option value="low">Price Low to High</option>--}}
                    {{--<option value="high">Price High to Low</option>--}}
                {{--</select>--}}
                {{--<input type="submit" value="Filter" >--}}
                {{--{!! Form::close() !!}--}}
            </div>
            {!! Form::open(['route' => 'products.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
            <div class="input-group">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Search product..', 'aria-describedby' => 'search']) !!}
                <span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
            </div>
            {!! Form::close() !!}
        </div>
        <div id="products" class="row list-group">
            @foreach($products as $product)
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <a href="{{url('products/'.$product->id)}}">
                            <img class="group list-group-image" src="img/{{$product->urlImage}}" alt=""/>
                        </a>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading"><a href="{{url('products/'.$product->id)}}">{{$product->name}}</a></h4>
                            <p class="group inner list-group-item-text">{{str_limit($product->description, 120)}}</p>
                            <div class="row">
                                <p class="price-list">{{$product->priceOutput}}</p>
                            </div>
                            <div class="row">
                                <div class="text-center col-xs-12 col-md-6">
                                    {!! Form::open(['route' => ['wishlist.update', $product->id],'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Add wish list
                                            </button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="text-center col-xs-12 col-md-6">
                                    <a class="btn btn-success" href="#">Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
